﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Divider
{
    class Program
    {
        static int[] simpleNumbers = ("2 3 5 7 11 13 17 19 23 29 31 37 41 43 47 53 59 61 67 71 73 79 83 89 97 101 103 107 109 " +
                "113 127 131 137 139 149 151 157 163 167 173 179 181 191 193 197 199 211 223 227 229 233 239 241 251 257 263 269 271 277 281 " +
                "283 293 307 311 313 317 331 337 347 349 353 359 367 373 379 383 389 397 401 409 419 421 431 433 439 443 449 457 461 463 " +
                "467 479 487 491 499 503 509 521 523 541 547 557 563 569 571 577 587 593 599 601 607 613 617 619 631 641 643 647 653 659 661 " +
                "673 677 683 691 701 709 719 727 733 739 743 751 757 761 769 773 787 797 809 811 821 823 827 829 839 853 857 859 863 877 881 " +
                "883 887 907 911 919 929 937 941947 953 967 971 977 983 991 997").Split(" ").Select(x => Convert.ToInt32(x)).ToArray();

        static void Main(string[] args)
        {
            int fC = 1;
            int fZ = 2;
            int sC = 1;
            int sZ = 5;
            (int, int) result = GetResult(fC, fZ, sC, sZ);
            Console.WriteLine(result.Item1);
            Console.WriteLine("-");
            Console.WriteLine(result.Item2);
        }

        static (int,int) GetResult(int fC, int fZ, int sC, int sZ)
        {
            if (fZ == sZ)
            {
                return GetEqualZResult(fC, fZ, sC, sZ);
            }
            return GetNotEqualZResult(fC, fZ, sC, sZ);
        }

        static (int, int) GetEqualZResult(int fC, int fZ, int sC, int sZ)
        {
            return (fC + sC, fZ);
        }

        static (int, int) GetNotEqualZResult(int fC, int fZ, int sC, int sZ)
        {
            int minimalCommonMultiple = FindMinimalCommonMultiple(fZ, sZ);
            (int, int) fsm = GetMultiplyer(fZ, sZ, minimalCommonMultiple);
            int firstMultiplier = fsm.Item1;
            int secondMultiplier = fsm.Item2;
            fC = fC * firstMultiplier;
            fZ = fZ * firstMultiplier;
            sC = sC * secondMultiplier;
            sZ = sZ * secondMultiplier;
            return GetEqualZResult(fC, fZ, sC, sZ);
        }

        static (int, int) GetMultiplyer(int fZ, int sC, int minimalCommonMultiple)
        {
            return (minimalCommonMultiple / fZ, minimalCommonMultiple / sC);
        }

        static int FindMinimalCommonMultiple(int fZ, int sZ)
        {
            return DecayNumber(fZ).Max() * DecayNumber(sZ).Max();
        }

        static List<int> DecayNumber(int number)
        {
            int start = number;           
            int simpleSize = simpleNumbers.Length;
            List<int> numbers = new List<int>();
            while (start > 1)
            {
                for (int i = 0; i < simpleSize; i++)
                {
                    int devideGarbage = start % simpleNumbers[i];
                    if (devideGarbage == 0)
                    {
                        numbers.Add(simpleNumbers[i]);
                        start = start / simpleNumbers[i];
                        break;
                    }
                }
            }
            return numbers;
        }
    }
}
